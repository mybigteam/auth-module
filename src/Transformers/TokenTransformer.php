<?php

namespace MyBigTeam\Auth\Transformers;

use MyBigTeam\Core\Transformers\BaseTransformer;

class TokenTransformer extends BaseTransformer
{
    /**
     * @return array
     */
    public function transform($token)
    {
        return [
            'id' => $token
        ];
    }
}