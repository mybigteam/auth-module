<?php

namespace MyBigTeam\Auth\Transformers;

use MyBigTeam\Core\Transformers\BaseTransformer;
use MyBigTeam\Auth\Auth\User;

class CurrentUserTransformer extends BaseTransformer
{
    /**
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
        ];
    }
}