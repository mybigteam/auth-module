<?php

namespace MyBigTeam\Auth\Providers;

use MyBigTeam\Core\Providers\BaseModuleServiceProvider;
use MyBigTeam\Core\Traits\GetModuleNameFromNamespace;
use MyBigTeam\Auth\Auth\User;
use Tymon\JWTAuth\Providers\LaravelServiceProvider as JWTAuthServiceProvider;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    use GetModuleNameFromNamespace;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        $this->mergeConfigFrom($this->path("config/jwt.php"), 'jwt');

        $this->bootAuth();
    }

    public function bootAuth()
    {
        $jwtProvider = new JWTAuthServiceProvider($this->app);
        $jwtProvider->boot();

        $overwrite = $this->app['config']->get('auth-module.overwrite_auth_config');

        if($overwrite) {
            $this->app['config']->set('auth.guards.api.driver', 'jwt');
            $this->app['config']->set('auth.providers.users.model', User::class);
        }
    }

    public function register()
    {
        $jwtProvider = new JWTAuthServiceProvider($this->app);
        $jwtProvider->register();
    }

    /**
     * @return string
     */
    public function getBaseDirectory()
    {
        return realpath(__DIR__.'/../..');
    }
}