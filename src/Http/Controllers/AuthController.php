<?php

namespace MyBigTeam\Auth\Http\Controllers;

use Illuminate\Http\Request;
use MyBigTeam\Core\Http\Controllers\BaseController;
use Tymon\JWTAuth\Facades\JWTAuth;
use MyBigTeam\Auth\Transformers\TokenTransformer;
use MyBigTeam\Auth\Transformers\CurrentUserTransformer;
use Illuminate\Auth\AuthenticationException;
use Auth;

class AuthController extends BaseController
{
    public function token(Request $request)
    {
        $credentials = [
            'email' => $request->input('data.attributes.username'),
            'password' => $request->input('data.attributes.password'),
        ];

        $token = JWTAuth::attempt($credentials);

        if(!$token) {
            throw new AuthenticationException("Invalid username or password");
        }

        return $this->response(
            $token,
            resolve(TokenTransformer::class)
        );
    }

    public function currentUser()
    {
        return $this->response(
            Auth::user(),
            resolve(CurrentUserTransformer::class)
        );
    }
}