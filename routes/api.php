<?php

Route::group([
    'prefix' => '/auth',
    'namespace' => 'MyBigTeam\Auth\Http\Controllers',
], function () {
    Route::post('/token', 'AuthController@token');

    Route::get('/current-user', 'AuthController@currentUser')
        ->middleware('auth:api');
});