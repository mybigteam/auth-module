<?php

namespace MyBigTeam\Auth\Tests;

use MyBigTeam\Core\Tests\TestCase as BaseTestCase;

class TestCase extends BaseTestCase
{
    /**
     * @return void
     */
    protected function getPackageProviders($app)
    {
        return array_merge(parent::getPackageProviders($app), [
            'MyBigTeam\Auth\Providers\ModuleServiceProvider',
        ]);
    }
}