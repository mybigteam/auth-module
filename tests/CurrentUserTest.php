<?php

use MyBigTeam\Auth\Tests\AuthenticatedTestCase;
use MyBigTeam\Core\Models\User;

class CurrentUserTest extends AuthenticatedTestCase
{
    public function testCurrentUser()
    {
        $this
            ->json('GET', 'api/v1/auth/current-user')
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'type' => 'current-user',
                    'attributes' => [
                        'email' => $this->user->email,
                    ]
                ],
            ]);
    }
}