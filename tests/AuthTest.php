<?php

use MyBigTeam\Auth\Tests\TestCase;
use MyBigTeam\Core\Models\User;

class AuthTest extends TestCase
{
    public function testToken()
    {
        $user = factory(User::class)->create([
            'email' => 'testuser@customemail.com',
        ]);

        $data = [
            'data' => [
                'type' => 'credential',
                'attributes' => [
                    'username' => 'testuser@customemail.com',
                    'password' => 'secret'
                ]
            ],
        ];

        $response = $this
            ->json('POST', 'api/v1/auth/token', $data, ['Origin' => 'http://teste'])
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'type' => 'token',
                    'attributes' => [
                    ]
                ],
            ])
            ->assertHeader('access-control-allow-origin', '*');

        $jsonResponse = $response->json();

        $token = $jsonResponse['data']['id'];

        $this->assertNotEmpty($token);

        return $token;
    }

    public function testCurrentUser()
    {
        $token = $this->testToken();

        $headers = [
            'Authorization' => "Bearer $token",
        ];

        $this
            ->json('GET', 'api/v1/auth/current-user', [], $headers)
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'type' => 'current-user',
                    'attributes' => [
                        'email' => 'testuser@customemail.com',
                    ]
                ],
            ]);
    }

    public function testCurrentUserWithoutToken()
    {
        $token = $this->testToken();

        $this->throwHttpExceptions = false;

        $this
            ->json('GET', 'api/v1/auth/current-user')
            ->assertStatus(401)
            ->assertJson([
                'errors' => [
                    [
                        'code' => 'authentication',
                        'detail' => 'Unauthenticated.',
                    ]
                ],
            ]);
    }
}