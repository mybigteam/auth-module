<?php

namespace MyBigTeam\Auth\Tests;

use Illuminate\Foundation\Testing\Concerns\MakesHttpRequests;
use MyBigTeam\Core\Models\User;

class AuthenticatedTestCase extends TestCase
{
    /**
     * @var MyBigTeam\Auth\Auth\User
     */
    public $user;

    /**
     * @var string
     */
    public $token;

    use MakesHttpRequests {
        transformHeadersToServerVars as realTransformHeadersToServerVars;
    }
    /**
     * Transform headers array to array of $_SERVER vars with HTTP_* format.
     *
     * @param  array  $headers
     * @return array
     */
    protected function transformHeadersToServerVars(array $headers)
    {
        if(!array_key_exists('Authorization', $headers)) {
            if(!$this->token) {
                $this->token = $this->getToken();
            }

            $headers['Authorization'] = "Bearer {$this->token}";
        }

        return $this->realTransformHeadersToServerVars($headers);
    }

    public function getToken()
    {
        $this->user = factory(User::class)->create();

        $data = [
            'data' => [
                'type' => 'credential',
                'attributes' => [
                    'username' => $this->user->email,
                    'password' => 'secret'
                ]
            ],
        ];

        $response = $this
            ->json('POST', 'api/v1/auth/token', $data, ['Authorization' => null])
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'type' => 'token',
                    'attributes' => [
                    ]
                ],
            ]);

        $jsonResponse = $response->json();

        $this->token = $jsonResponse['data']['id'];

        $this->assertNotEmpty($this->token);

        return $this->token;
    }

    /**
     * Create the test response instance from the given response.
     *
     * @param  \Illuminate\Http\Response  $response
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function createTestResponse($response)
    {
        return parent::createTestResponse($response);
    }
}